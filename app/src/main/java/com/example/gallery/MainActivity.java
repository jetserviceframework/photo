package com.example.gallery;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.provider.MediaStore;
import android.content.ContentResolver;
import android.database.Cursor;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends Activity {
    private GridView gw;
    private ImageView imageView;
    private TextView description1;
    private TextView description2;
    private Button button_byname;
    private Button button_bydate;
    private LinearLayout leftbar;
    private LinearLayout descriptbar;
    private LinearLayout titlebar;
    private List<String> imageID;
    private List<String> imagePaths;
    private List<String> imageTitle;
    private List<String> imagedate;
    private ImageAdapter imageAdapter;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        gw = (GridView) findViewById(R.id.GridView01);
        imageView = (ImageView) findViewById(R.id.imageView1);
        description1 = (TextView) findViewById(R.id.description_cout);
        description2 = (TextView) findViewById(R.id.description_date_name);

        leftbar = (LinearLayout) findViewById(R.id.leftbar);
        descriptbar = (LinearLayout) findViewById(R.id.description);
        titlebar = (LinearLayout) findViewById(R.id.titlebar);

        button_byname = (Button) findViewById(R.id.button);
        button_bydate = (Button) findViewById(R.id.button2);

        imageID = new ArrayList<String>();
        imagePaths = new ArrayList<String>();
        imageTitle = new ArrayList<String>();
        imagedate = new ArrayList<String >();

        final ContentResolver cr = getContentResolver();


        final String[] projection = {MediaStore.Images.Thumbnails._ID, MediaStore.Images.Thumbnails.DATA, MediaStore.Images.Media.TITLE, MediaStore.Images.Media.DATE_MODIFIED};

        button_byname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Cursor cursor = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, MediaStore.Images.Media.TITLE);
                imageID.clear();
                imagePaths.clear();
                imagedate.clear();
                imageTitle.clear();
                for (int i = 0; i < cursor.getCount(); i++) {

                    cursor.moveToPosition(i);
                    int id = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Media._ID));
                    String filepath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                    String title = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.TITLE));
                    Date date = new Date(cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media.DATE_MODIFIED))*1000);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                    String dateString = sdf.format(date);

                    imageID.add(id + "");
                    imagePaths.add(filepath);
                    imageTitle.add(title);
                    imagedate.add(dateString);

                }
                imageAdapter.notifyDataSetChanged();
                description1.setText("1/" + imageTitle.size());
                description2.setText(imagedate.get(0)+" " + imageTitle.get(0));
            }

        });

        button_bydate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Cursor cursor = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, MediaStore.Images.Media.DATE_MODIFIED+" DESC");
                imageID.clear();
                imagePaths.clear();
                imagedate.clear();
                imageTitle.clear();
                for (int i = 0; i < cursor.getCount(); i++) {

                    cursor.moveToPosition(i);
                    int id = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Media._ID));
                    String filepath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                    String title = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.TITLE));
                    Date date = new Date(cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media.DATE_MODIFIED))*1000);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                    String dateString = sdf.format(date);

                    imageID.add(id + "");
                    imagePaths.add(filepath);

                    imageTitle.add(title);
                    imagedate.add(dateString);
                }
                imageAdapter.notifyDataSetChanged();
                description1.setText("1/" + imageTitle.size());
                description2.setText(imagedate.get(0)+" " + imageTitle.get(0));

            }

        });



        Cursor cursor = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, null);

        for (int i = 0; i < cursor.getCount(); i++) {

            cursor.moveToPosition(i);
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Media._ID));
            String filepath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            String title = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.TITLE));
            Date date = new Date(cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media.DATE_MODIFIED))*1000);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            String dateString = sdf.format(date);
            imageID.add(id + "");
            imagePaths.add(filepath);
            imageTitle.add(title);
            imagedate.add(dateString);
        }
        cursor.close();

        imageAdapter = new ImageAdapter(MainActivity.this, imagePaths);
        gw.setAdapter(imageAdapter);
        imageAdapter.notifyDataSetChanged();
        description1.setText( "1/" + imageTitle.size());
        description2.setText(imagedate.get(0)+" " + imageTitle.get(0));

        imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Log.i("Vera", "MainActivity onClick");
                imageView.setVisibility(View.GONE);
                gw.setVisibility(View.VISIBLE);
                leftbar.setVisibility(View.VISIBLE);
                descriptbar.setVisibility(View.VISIBLE);
                titlebar.setVisibility(View.VISIBLE);
            }

        });
        imageView.setVisibility(View.GONE);

        gw.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                description1.setText(String.valueOf(position+1) + "/" + imageTitle.size());
                description2.setText(imagedate.get(position)+" " + imageTitle.get(position));
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    public void setImageView(int position) {
        Log.i("Vera", "MainActivity setImageView");
        Bitmap bm = BitmapFactory.decodeFile(imagePaths.get(position));
        imageView.setImageBitmap(bm);
        imageView.setVisibility(View.VISIBLE);
        gw.setVisibility(View.GONE);
        leftbar.setVisibility(View.GONE);
        descriptbar.setVisibility(View.GONE);
        titlebar.setVisibility(View.GONE);

    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.gallery/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.gallery/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }


}


