package com.example.gallery;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by vera on 2015/12/9.
 */
public class ImageAdapter extends BaseAdapter {
    private ViewGroup layout;
    private Context context;
    private List coll;

    public ImageAdapter(Context context, List coll) {

        super();
        this.context = context;
        this.coll = coll;
    }

    @Override
    public int getCount() {
        return coll.size();
    }

    @Override
    public Object getItem(int position) {
        return coll.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowview = inflater.inflate(R.layout.item_photo, parent, false);
        layout = (ViewGroup) rowview.findViewById(R.id.rl_item_photo);
        ImageView imageView = (ImageView) rowview.findViewById(R.id.imageView2);

        DisplayMetrics dm = context.getResources().getDisplayMetrics();


        float dd = dm.density;
        float px = 25 * dd;
        int newWidth = (int) (dm.widthPixels - px) / 4;
        int newHeigh = (int) (dm.heightPixels - px) / 4;
        layout.setLayoutParams(new GridView.LayoutParams(newWidth, newWidth));

         Bitmap bm = BitmapFactory.decodeFile((String) coll.get(position));
         Bitmap newBit = Bitmap.createScaledBitmap(bm, newWidth, newWidth, true);


//        int w = bm.getWidth();
//        int h = bm.getHeight();
//        int wh = w > h ? h : w;
//        int retX = w > h ? (w - h) / 2 : 0;
//        int retY = w > h ? 0 : (h - w) / 2;
//        Bitmap newBit =  Bitmap.createBitmap(bm, retX, retY, wh, wh, null, false);

        imageView.setImageBitmap(newBit);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE  );

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                ((MainActivity) context).setImageView(position);
            }

        });

        return rowview;
    }
}
